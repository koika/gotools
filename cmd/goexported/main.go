package main

import (
	"fmt"
	"go/ast"
	"go/parser"
	"go/token"
	"log"
	"os"
)

var fset = token.NewFileSet()

func main() {
	pkgs, err := parser.ParseDir(fset, os.Args[1], nil, 0)
	if err != nil {
		log.Fatal(err)
	}

	for _, pkg := range pkgs {
		ast.PackageExports(pkg)
		for _, file := range pkg.Files {
			if !ast.FileExports(file) {
				continue
			}
			ast.Inspect(file, func(node ast.Node) bool {
				// fmt.Printf("GOT %T\n", node)
				// format.Node(os.Stdout, fset, node)
				// fmt.Print("\n")
				switch n := node.(type) {
				case *ast.File:
					return true
				case *ast.FuncDecl:
					if n.Recv == nil {
						fmt.Printf("%s\n", n.Name)
					}
				case *ast.GenDecl:
					return n.Tok == token.VAR || n.Tok == token.CONST
				case *ast.ValueSpec:
					for _, ident := range n.Names {
						fmt.Println(ident.String())
					}
					return false
				case *ast.Ident:
					if ast.IsExported(n.Name) {
						fmt.Println(n.Name)
					}

					// case *ast.Ident:
					// 	continue
					// 	if ast.IsExported(n.Name) {
					// 		fmt.Printf("%s\n", n.Name)
					// 	}
				default:
					return false
				}
				return false

			})
			// ast.Walk(visitfunc(printExportedIdent), file)
		}
	}

}

func printExportedIdent(node ast.Node) ast.Visitor {
	switch n := node.(type) {
	case *ast.Ident:
		if ast.IsExported(n.Name) {
			fmt.Printf("%s\n", n.Name)
		}
	}
	return visitfunc(printExportedIdent)
}

type visitfunc func(ast.Node) ast.Visitor

func (f visitfunc) Visit(n ast.Node) ast.Visitor {
	return f(n)
}
